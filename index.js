
const server = () => {
	const compression = require('compression')
	const express = require('express')
	const app = express()
	app.use(compression())
	app.use(express.static('public'))
	const users = require('./routes/users').users
	users(app)

	const start = (port) => {
		pushMessages(port)
	}

	const pushMessages = (port) => {
		const server = require('http').createServer()
		  	, url = require('url')
		  	, WebSocketServer = require('ws').Server
		  	, wss = new WebSocketServer({ server: server })
		  	
		 
		app.use((req, res) => {
		  res.send({ msg: "hello" });
		});
		 
		wss.on('connection', (ws) => {

			const location = url.parse(ws.upgradeReq.url, true);
		  	// you might use location.query.access_token to authenticate or share sessions 
		  	// or ws.upgradeReq.headers.cookie (see http://stackoverflow.com/a/16395220/151312) 
		 
			ws.on('message', (message) => {
				console.log('received: %s', message);
			});
			
			setInterval(() => {
			 	ws.send('server echo: ' + new Date());
			}, 1000)
		  
		});
		 
		server.on('request', app);

		server.listen(port, () => { 
			console.log('Listening on ' + server.address().port) 
		})
	}

	return {
		start
	}

}

server().start(4080)


