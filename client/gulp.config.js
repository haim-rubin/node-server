/* eslint-disable */
var root = './';
var client = root ;
var clientApp = client + 'app/';
var clientComponents = client + 'components/';
var dest = '../' + 'public';
var kendoAssetsBasePath = './src/client/components/webui-core/vendor/kendoui.customizations/';

var config = {
    paths: {
        root: root,
        client: client,
        apps: clientApp,
        components: clientComponents,
        dest: dest
    },
    styles: {
        less: [
            clientApp + 'style-reset.less',
            clientApp + 'base.less',
            clientApp + 'app.less',
            clientApp + 'container.less',
            clientComponents + '**/*.less',
            '!' + clientComponents + '**/*vendor*/**/*.less'
        ],
        bundle: 'app.css',
    },
    scripts: {
        rootApps: [
            clientApp + 'app.js'
        ]
    },
    assets: {
        sources: [
            clientApp + '**/assets/**/*.*',
            clientComponents + '**/assets/**/*.*'
        ],
        dest: dest + 'assets/'
    },
    static: {
        sources: [clientApp + 'index.html', clientApp + 'dashboard-ap', clientApp + 'dashboard-app/**/*'],
        dest: dest
    },
    watch: {
        app: clientApp + '**/*.js',
        components: [
            clientComponents + '**/*.js',
            clientComponents + '**/*.jsx'
            
        ],
        html: [client + '**/*.tmpl.html']
    },
    vendor: {
        scripts: {
            sources: [
                './node_modules/es6-promise/dist/es6-promise.min.js',
                './node_modules/jquery/dist/jquery.min.js',
                './node_modules/lodash/index.js',
                './node_modules/angular/angular.min.js',
                './node_modules/moment/min/moment.min.js',
                './node_modules/operative/dist/operative.min.js',
                './node_modules/webui-popover/src/jquery.webui-popover.js',
                './node_modules/angular-gridster/dist/angular-gridster.min.js',
                './node_modules/whatwg-fetch/fetch.js',
                './node_modules/fetch-intercept/index.js'

            ],
            dest: dest,
            bundle: 'vendor.js'
        },
        scriptsCopy: {
            sources: [
                './node_modules/babel-polyfill/dist/polyfill.min.js',
                './node_modules/es6-promise/es6-promise.min.js',
                './node_modules/operative/dist/operative.min.js',
                './node_modules/moment/min/moment.min.js',
                './node_modules/whatwg-fetch/fetch.js',
                './node_modules/fetch-intercept/index.js'
            ],
            dest: dest + 'scripts/'
        },
        styles: {
            sources: [
                './node_modules/angular/angular-csp.css',
                './node_modules/webui-popover/src/jquery.webui-popover.css',
                './node_modules/angular-gridster/dist/angular-gridster.min.css',
            ],
            bundle: 'vendor.css'
        },
        kendo: {
            customStyles: [
                './src/client/components/webui-core/vendor/kendoui.customizations/kendo.common-material.less',
                './src/client/components/webui-core/vendor/kendoui.customizations/kendo.material.less'
            ],
            stylesBundle: 'kendo.material-custom.css',
            dest: './src/client/components/webui-core/vendor/kendoui.customizations',
            script: './src/client/components/webui-core/vendor/kendoui/js/kendo.all.min.js',
            basePath: kendoAssetsBasePath,
            assets: [
                kendoAssetsBasePath + 'images/**/*',
                kendoAssetsBasePath + 'Material/**/*',
                kendoAssetsBasePath + 'textures/**/*',
                kendoAssetsBasePath + 'fonts/**/*'
            ]
        }
    }
};

module.exports = config;
