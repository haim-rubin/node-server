import {React, Component, ReactDOM} from '../ui-core/vendor/common';

class Index extends Component {
 
	constructor(props){
		super(props); 
		this.state = {server: ''}
		
	}

	listenToServer() {
		const ws = new WebSocket('ws://127.0.0.1:4080');
		
		ws.onopen = () => {
		    ws.send('something');
		}

		ws.onmessage = (message) => {
		    this.setState({server: message.data});
		};
	}

	componentDidMount() {
		this.listenToServer()
	}
	
	render(){	
		return(
			<div className='react-index'>
				<div>{this.state.server}</div>
			</div>
			);
	}
}

export default Index