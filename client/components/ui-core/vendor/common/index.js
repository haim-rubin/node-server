import uuid from 'node-uuid';
import React from 'react';
import ReactDOM from 'react-dom'
import warning from '../../../../node_modules/warning';
import classnames from '../../../../node_modules/classnames';
const Component = React.Component;

export {uuid,
        React,
        ReactDOM,
        Component,
        warning,
        classnames};
