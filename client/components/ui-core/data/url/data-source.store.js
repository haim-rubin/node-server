import queryString from 'query-string';

const storeSpec = {
    init () {

    },
    getData (param = null) {
        return new Promise((resolve, reject) => {
            try {
                const parsed = queryString.parse(location.search);

                if (param !== null) {
                    if (!(param in parsed)) {
                        reject('Missing parameter in query string : ' + param);
                    }

                    resolve(parsed[param]);
                }

                resolve(parsed);
            }
            catch (error) {
                reject(error);
            }
        });
    },
    onGetData (param = null) {
        return this.getData(param)
    }
};

export default storeSpec;
