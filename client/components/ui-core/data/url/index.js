import actions from './data-source.actions';
import store from './data-source.store';

module.exports = { actions, store };
