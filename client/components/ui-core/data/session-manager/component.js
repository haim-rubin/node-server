
import { EventEmitter, merge } from '../../vendor/common';
import LocalStorage from '../local-storage';
import {httpClient} from '../http-client';
const singleton = Symbol.for('SESSION-MANAGER');
const singletonEnforcer = Symbol();


export default (function (global) {
    class SessionManager extends EventEmitter {
        constructor (enforcer) {
            if (enforcer !== singletonEnforcer) {
                throw 'Cannot construct SessionManager; see documentation.';
            }

            super();

            this.userData = {              
                username: null,
                role: null,
                email: null,
                icon: null,
                token: null
            };

            this.sessionId = null;

            this.timestamps = {
                lastActivity: null,
                loggedIn: null,
                loggedOut: null
            };           
        }

        getUserInfo(){
            return {...this.userData};
        }

        extractToken() {
            const token = LocalStorage.getItem('token');
            token && this.setToken(token);
            return token;
        }

        invalidateSession () {
            this.timestamps.loggedIn = null;
            this.timestamps.loggedOut = new Date();
            this.timestamps.lastActivity = new Date();
            this.sessionId = null;
            this.userData = { username: null, role: null, email: null, icon: null, token: null };
            this.emit('userInvalid');
        }

        setSession ({ sessionId, userData }) {
            this.sessionId = sessionId;
            this.userData = merge({}, userData);
            this.timestamps.loggedIn = new Date();
            this.timestamps.lastActivity = new Date();
            this.emit('userValid');
        }

        setToken(token){
            this.userData.token = token;
            LocalStorage.setItem('token', token);
        }

        getToken(token){
            return this.userData.token;
        }

        logout (url) {
       
            const dataSource = merge({}, httpClient, {
                options: {
                    method: 'POST',
                    url: '/logout'
                }
            });

            return dataSource
                .getData()
                .then(response => (response.data))
                .then(data => {
                    return data;
                });
        }

        setUserData(user){
            this.userData = merge({}, this.userData, user);
        }

        getUserData () {
            const dataSource = merge({}, httpClient, {
                options: {
                    method: 'GET',
                    url: '/authenticate-user'
                }
            });

            return dataSource
                .getData()
                .then(response => (response.data))
                .then(data => {
                    this.setUserData(data && data.user);
                    return data && data.user;
                });
        }

        logOutRedirect (url) {
            window.location = url;
        }

        static get instance () {
            if (!global[singleton]) {
                global[singleton] = new SessionManager(singletonEnforcer);
            }
            return global[singleton];
        }

        static get sessionId () {
            return SessionManager.instance.sessionId;
        }

        static get userData () {
            return SessionManager.instance.userData;
        }

        static get lastActivityOn () {
            return SessionManager.instance.timestamps.lastActivity;
        }

        static get isAuthenticated () {
            return SessionManager.instance.userData.username !== null;
        }

        static get loggedInOn () {
            return SessionManager.instance.timestamps.loggedIn;
        }

        static get loggedOutOn () {
            return SessionManager.instance.timestamps.loggedOut;
        }

        static extractToken() {
            SessionManager.instance.extractToken();
        }

        static setToken(token){
            SessionManager.instance.setToken(token);            
        }

        static getToken(){
            return SessionManager.instance.getToken();            
        }

        static logout () {
           return SessionManager.instance.logout();
        }

        static authenticationError () {
            SessionManager.instance.invalidateSession();
        }

        static sessionTimeout () {
            SessionManager.instance.invalidateSession();
        }

        static userAuthenticated ({ sessionId, userData }) {
            SessionManager.instance.setSession({ sessionId, userData });
        }

        static getUserInfo () {
            return SessionManager.instance.getUserInfo();
        }

         static getUserData () {
            return SessionManager.instance.getUserData();
        }
    }

    return SessionManager;
}(typeof window !== 'undefined' ? window : this));
