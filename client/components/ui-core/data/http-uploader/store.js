const storeSpec = {    
  options: {
      method: 'POST',
      url: '',
      credentials: null
  },
  getFormData(file){
    const formData = new FormData();
    formData.append('file',  file, file.name);   
    return formData;      
  },
  addEventListener(xmlHttpRequest, delegates){
      xmlHttpRequest.upload.addEventListener("progress", delegates.progress, false);
      xmlHttpRequest.addEventListener("load", delegates.complete, false);
      xmlHttpRequest.addEventListener("error", delegates.failed, false);
      xmlHttpRequest.addEventListener("abort", delegates.canceled, false);
  },
  upload (file, delegates) {   
    const xmlHttpRequest = new XMLHttpRequest();
    this.addEventListener(xmlHttpRequest, delegates);
    xmlHttpRequest.open(this.options.method, this.options.url);
    const formData = this.getFormData(file);
    xmlHttpRequest.send(formData);
  }
};
 export default storeSpec; 
