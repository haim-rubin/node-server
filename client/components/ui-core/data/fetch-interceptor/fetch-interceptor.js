const singleton = Symbol.for('FETCHINTERCEPT');
import FetchIntercept  from '../../../../../../node_modules/fetch-intercept';
import {devLog} from '../../aspect/logger';

class FetchInterceptComponent {

    constructor () {
       this.init();
    }

    init(){
        this._reset();
        this._unregisterInspector();
        this._registerInspector();
    }

    onRequest (url, config) {
        let paramsRef = {url, config},
            params = this.definitions.request? this._invoke(this.definitions.request, paramsRef) : paramsRef;

        return  params? [params.url, params.config] : [paramsRef.url, paramsRef.config];
    }

    onRequestError (error) {
        //Here logic
        return Promise.reject(error);
    }
    onResponse (response) {
        this.definitions.statuses[response.status] && this._bulkInvoke(this.definitions.statuses[response.status], response);
        this.definitions.any && this._bulkInvoke(this.definitions.any, response);
        return response;
    }

    onResponseError (error) {
        this.definitions.error && this._bulkInvoke(this.definitions.error, error);
        // Handle an fetch error
        return Promise.reject(error);
    }

    register (definitions) {
        this._mapStatuses(definitions);
    }

    registerStatus (definition) {
        this._mapStatus(definition)
    }

    unregister () {
        this._reset();
    }

    unregisterStatus (identifier) {
        const handlerIndex = this.definitions.statuses[identifier].indexOf(identifier.handler);
        if(handlerIndex > -1){
            this.definitions.statuses[identifier].splice(handlerIndex, 1);
        }
    }

    _getAsArray(obj){
        return Array.isArray(obj)? obj : [obj];
    }

    _reset() {
        this.definitions = {
            any: null,
            error: null,
            statuses: {},
            request: null
        };
    }

    _mapStatuses(definitions){
        this._reset();
        this.definitions.any = this._getAsArray(definitions.any);
        this.definitions.error = definitions.error?
                                    this._getAsArray(definitions.error) :
                                    undefined;

        this._getAsArray(definitions.statuses)
            .forEach(status => {
                this._mapStatus(status);
            }
        );
        this.definitions.request = definitions.request.handler;
    }

    _mapStatus(status){
        this.definitions.statuses[status.code] = this.definitions.statuses[status.code] || [];
        this.definitions.statuses[status.code] = this.definitions.statuses[status.code].concat(status.handler);
    }

    _bulkInvoke(list, params){
        list.forEach(method => {
            this._invoke(method, params);
        });
    }

    _invoke(method, params){
        if(typeof method === 'function'){
            return method.apply(method, Array.isArray(params)? params : [params]);
        }
    }

    _unregisterInspector(){
        this._invoke(this._innerUnregisterInspector);
    }

    _registerInspector(){
        let self = this;
        this._innerUnregisterInspector =
            FetchIntercept.register({
                request: (url, config) => {
                    return self.onRequest(url, config);
                },
                requestError: (error) => {
                    return self.onRequestError(error);
                },
                response: (response) => {
                    return self.onResponse(response);
                },
                responseError: (error) => {
                    // Handle an fetch error
                    return self.onResponseError(error);
                }
            });
    }
}

export default (function (global) {
    return class Interceptor {
        constructor () {
        }

        static get instance () {
            if (!global[singleton]) {
                global[singleton] = new FetchInterceptComponent();
            }

            return global[singleton];
        }

        static register (definitions) {
            return Interceptor.instance.register(definitions);
        }

        static registerStatus (definition) {
            Interceptor.instance.registerStatus(definition);
        }

        static unregisterStatus (identifier) {
            Interceptor.instance.unregisterStatus(identifier);
        }

        static unregister () {
            Interceptor.instance.unregister();
        }
    }
}(typeof window !== 'undefined' ? window : this));
