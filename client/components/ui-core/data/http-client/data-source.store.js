const storeSpec = {    
    options: {
        method: 'GET',
        url: '',
        credentials: null, // sessionId / token / HTTP basic auth hash,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        logging: false
    },   
    getData (context) {      
        let requestOptions = {
            method: this.options.method,
            headers: this.options.headers,
            credentials: this.options.credentials,
            body: JSON.stringify(context)
        };

        return fetch(this.options.url, requestOptions)
            .then(checkStatus)
            .then(response => response.json())
            .then(function (data) {
                const state = {
                    timestamp: new Date(),
                    status: 'ok',
                    message: null,
                    data: data
                };
               
                return state;
            }.bind(this))
            .catch(function (error) {
                const state = {
                    timestamp: new Date(),
                    status: error.response.status,
                    message: error.response.statusText,
                    data: null                    
                };
                return state;
            }.bind(this));
    }
};

// TODO process error contract as defined by the backend team
const checkStatus = (response) => {
    if (response.status >= 200 && response.status < 300) {
        return response;
    }
    else {
        var error = new Error(response.statusText);
        error.response = response;
        throw error;
    }
};

export default storeSpec;
