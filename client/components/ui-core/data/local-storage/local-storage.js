const singleton = Symbol.for('LOCALSTORAGE');

class LocalStorageComponent {
    constructor (localStorage, JSON) {
        this.localStorage = localStorage;
        this.JSON = JSON;
    }

    getItem (key) {
        let value = this.localStorage.getItem(key);
        return this.JSON.parse(value);
    }

    removeItem (key) {
        this.localStorage.removeItem(key);
    }

    setItem (key, value) {
        let jsonValue = this.JSON.stringify(value);
        this.localStorage.setItem(key, jsonValue);
    }
}

export default (function (global) {
    return class LocalStorage {
        constructor () {

        }

        static get instance () {
            if (!global[singleton]) {
                global[singleton] = new LocalStorageComponent(global.localStorage, global.JSON);
            }

            return global[singleton];
        }

        static getItem (key) {
            return LocalStorage.instance.getItem(key);
        }

        static removeItem (key) {
            LocalStorage.instance.removeItem(key);
        }

        static setItem (key, value) {
            LocalStorage.instance.setItem(key, value);
        }
    }
}(typeof window !== 'undefined' ? window : this));
