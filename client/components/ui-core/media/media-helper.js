import fileType from 'file-type';

const MediaHelper = () => {
	const fileMapper = {
		'1196314761': 'image/png',
		'944130375': 'image/gif',
        '544099650': 'image/bmp',
        '-520103681': 'image/jpg'
    };

    const getFileTypeFormBlobUrl = (fileUrl) => {

    	return new Promise((resolve, reject) => {
    		const xmlHttpRequest = new XMLHttpRequest();
			xmlHttpRequest.responseType = 'arraybuffer';

			xmlHttpRequest.onload = function() {
				const fileTypeResponse = fileType(new Uint8Array(xmlHttpRequest.response));
				console.log(fileTypeResponse);
				resolve(fileTypeResponse);
			};

			xmlHttpRequest.open('GET', fileUrl);
			xmlHttpRequest.send();
		});
    };


	const getImageType = (file) => {
    	const reader = new FileReader();
		const promise = new Promise((resolve, reject) => {
      		reader.onload = (event) => {
	        	const fileType = fileMapper[new Int32Array(reader.result)[0]];
        		resolve(fileType);
    		}; 
    	});		
        reader.readAsArrayBuffer(file);
        return promise;
	};

	const readTextFile = (file) => {
		const fileReader = new FileReader();

		const promise = new Promise((resolve, reject) => {
	      fileReader.addEventListener("load", (event) => {
	        const content = event.target.result;
	        resolve(content);
	      });
	  	});
      	fileReader.readAsText(file);
      	return promise;
	};

	const getImageRatio = (sizes) => {
		sizes = {
			max: {
				width: 400,
				height: 150
			},
			image: {
				width: 358,
				height: 141
			}
		};


        let ratio = 0;  // Used for aspect ratio
        let width = $(this).width();    // Current image width
        let height = $(this).height();  // Current image height

        // Check if the current width is larger than the max
        if(width > sizes.max.width){
            ratio = sizes.max.width / width;   // get ratio for scaling image
            height = height * ratio;    // Reset height to match scaled image
            width = width * ratio;    // Reset width to match scaled image
        }

        // Check if current height is larger than max
        if(height > sizes.max.height){
            ratio = sizes.max.height / height; // get ratio for scaling image
            width = width * ratio;    // Reset width to match scaled image
            height = height * ratio;    // Reset height to match scaled image
        }

	}

	return {
		getImageType: getImageType,
		readTextFile: readTextFile,
		getFileTypeFormBlobUrl: getFileTypeFormBlobUrl
	};
};

export default MediaHelper();