
const invokeIfFunction = (method, args, self) => {
	typeof method === 'function' && method.apply(self, args);
};

export default {invokeIfFunction}