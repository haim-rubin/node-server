import Rx from 'rx'

const requestStream = Rx.Observable.just('https://api.github.com/users')

const responseStream = requestStream
    .flatMap(url => 
        Rx.Observable.fromPromise(
            fetch(url, {
                method: 'get'
            })
            .then(response => response.json())
        )   
    )

export default responseStream