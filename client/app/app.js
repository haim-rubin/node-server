import {ReactDOM, React} from '../components/ui-core/vendor/common';
import {Index} from '../components/index';
import responseStream from '../api/gallery-store'

responseStream.subscribe(response => {
    console.log(response)
})
ReactDOM.render(<Index />, document.getElementById('container'));