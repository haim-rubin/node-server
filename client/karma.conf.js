'use strict';

module.exports = function karmaConfig (config) {
    config.set({
        basePath: './',
        frameworks: ['jasmine', 'jasmine-matchers', 'browserify'],
        plugins: [
            require('karma-jasmine'),
            require('karma-jasmine-matchers'),
            require('karma-browserify'),
            require('karma-phantomjs-launcher'),
            // require('karma-chrome-launcher'),
            require('karma-spec-reporter'),
            require('karma-sourcemap-loader')
        ],
        files: [
            './node_modules/jasmine-collection-matchers/index.js',
            './node_modules/babel-polyfill/dist/polyfill.js',
            './node_modules/moment/min/moment.min.js',
            './node_modules/jquery/dist/jquery.min.js',
            './src/client/**/*.spec.js'
        ],
        exclude: [
            // './src/client/components/appwall.security-monitoring.common/*.*'
        ],
        preprocessors: {
            './src/client/**/*.spec.js': ['browserify']
        },
        reporters: ['spec'],
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        singleRun: false,
        browsers: ['PhantomJS'],
        browserify: {
            debug: true,
            transform: [
                [
                    'babelify', {
                        presets: ['es2015', 'stage-0', 'react'],
                        plugins: ['transform-es2015-modules-commonjs', 'transform-runtime'],
                        ignore: ['/node_modules/']
                    }
                ],
                'stringify'
            ]
        }
    });
};
