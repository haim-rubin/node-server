
var gulp = require('gulp');
var del = require('del');
var browserify = require('browserify');
var babelify = require('babelify');
var stringify = require('stringify');
var buffer = require('vinyl-buffer');
var source = require('vinyl-source-stream');
var through = require('through2');
var $ = require('gulp-load-plugins')({ lazy: true });
var config = require('./gulp.config');
var PostCSSWriteSVG = require('postcss-write-svg');

var noop = function () {
};

gulp.task('clean', function (done) {
    del([config.paths.dest + '**/*'], done);
});

gulp.task('test:api-compliance', noop);

gulp.task('vendor:common:scripts', function () {
    return gulp.src(config.vendor.scripts.sources)
        .pipe($.plumber({ errorHandler: reportError }))
        .pipe($.concat(config.vendor.scripts.bundle))
        .pipe(gulp.dest(config.vendor.scripts.dest));
});

gulp.task('vendor:common:styles', function () {
    return gulp.src(config.vendor.styles.sources)
        .pipe($.sourcemaps.init({ loadMaps: true }))
        .pipe($.base64())
        .pipe($.concat(config.vendor.styles.bundle))
        .pipe($.sourcemaps.write())
        .pipe(gulp.dest(config.paths.dest));
});

gulp.task('vendor:common:assets', noop);

gulp.task('vendor', ['vendor:common'], function () {
    // TODO wire up HTML?
});

gulp.task('npm:bump', noop);

gulp.task('npm:publish', ['npm:bump'], noop);

gulp.task('app:scripts', function () {
    var browserifyDefaults = {
        debug: true,
        transform: [
            ['babelify', {
                presets: ["es2015", "react", "stage-2"],
                plugins: [
                    "babel-plugin-transform-decorators-legacy"
                ]}
            ],
            ['stringify', { minify: true }],
            ['browserify-ngannotate', { add: true, remove: true, single_quotes: true }]
        ]
    };

    var browserified = function () {
        return through.obj(function (chunk, enc, callback) {
            if (chunk.isBuffer()) {
                chunk.contents =
                    browserify(Object.assign({}, browserifyDefaults, { entries: chunk.path }))
                        .bundle()
                        .on('error', function (error) {
                            console.log(error.message);
                        })
                        .pipe(source(chunk.path))
                        .pipe($.flatten())
                        .pipe($.rename({ extname: '.bundle.js' }))
                        .pipe(buffer())
                        .pipe($.sourcemaps.init({ loadMaps: true }))
                        .pipe($.sourcemaps.write('./'))
                        .pipe(gulp.dest(config.paths.dest));

                this.push(chunk);
            }

            callback();
        });
    };

    return gulp.src(config.scripts.rootApps)
        .pipe($.plumber({ errorHandler: reportError }))
        .pipe(browserified());
});

gulp.task('app:styles', function () {
    return gulp.src(config.styles.less)
        .pipe($.plumber({ errorHandler: reportError }))
        .pipe($.sourcemaps.init())
        //.pipe($.less())
       // .pipe($.base64())
        .pipe($.postcss([PostCSSWriteSVG()]))
        .pipe($.autoprefixer({ map: true, browsers: ['last 2 versions'] }))
        .pipe($.concat(config.styles.bundle))
        .pipe($.sourcemaps.write())
        .pipe(gulp.dest(config.paths.dest));
});

gulp.task('app:assets', function () {
    return gulp.src(config.assets.sources)
        .pipe($.plumber({ errorHandler: reportError }))
        .pipe($.flatten({ includeParents: -1 }))
        .pipe(gulp.dest(config.assets.dest));
});

gulp.task('app:static', function () {
    return gulp.src(config.static.sources)
        .pipe($.plumber({ errorHandler: reportError }))
        .pipe(gulp.dest(config.static.dest));
});

gulp.task('app', ['app:styles', 'app:scripts', 'app:assets', 'app:static']);

gulp.task('build:dev', ['app']);

gulp.task('build:prod', noop);

gulp.task('build:webui-core', ['vendor'], function () {
});

gulp.task('build:webui-archetypes', ['build:webui-core'], noop);

gulp.task('watch', ['build:dev'], function () {
    gulp.watch([config.watch.app], ['app:scripts']);
    gulp.watch([config.watch.components], ['app:scripts']);
    gulp.watch([config.watch.html], ['app:scripts']);
    gulp.watch([config.static.sources], ['app:static']);
    gulp.watch([config.styles.less], ['app:styles']);
});

function reportError (error) {
    $.notify({
        title: 'Gulp Task Error',
        message: 'Check the console.'
    }).write(error);

    console.log(error.toString());

    this.emit('end');
}
